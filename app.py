from flask import Flask, send_from_directory
import random

app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    if ('.' in path):
        return send_from_directory('client/public', path)
    return send_from_directory('client/public', 'index.html')

@app.route("/rand")
def hello():
    return str(random.randint(0, 100))

if __name__ == "__main__":
    app.run()
